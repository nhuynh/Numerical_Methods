#!usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import math
import scipy.special as sci

plt.close('all')
# Solving for the Bessel Equation
nvec = np.asarray([100,200,400])
a = 0
b = 8.10
ya = 0
yb = 5.90

errorvec = np.zeros((len(nvec),1))
plt.figure()
for ndx in range(0,len(nvec)):
    n = nvec[ndx]
    xvec = np.linspace(a,b,n)
    dx = (b-a)/(n-1)
    A = np.zeros((n,n))
    A[0,0] = 1
    A[n-1,n-1] = 1

    RHS = np.zeros((n,1))
    RHS[0] = 0
    RHS[n-1] = 5.90

    for k in range(1,n-1):
        x = xvec[k]
        ypp = math.pow(x,2)
        yp  = x
        y   = math.pow(x,2) - 16.0
        A[k,k] = (-2*ypp)/(math.pow(dx,2)) + y
        A[k,k-1] = ypp/(math.pow(dx,2)) - yp/(2*dx)
        A[k,k+1] = ypp/(math.pow(dx,2)) + yp/(2*dx)
        RHS[k]   = 0


    yvec = np.linalg.solve(A,RHS)       # Solves inverse(A)*RHS

    c = yb/sci.jv(4,b)
    ytrue = c*sci.jv(4,xvec)
    ytrue = np.transpose(ytrue)
    error = np.abs(ytrue - yvec)
    error = np.sum(error)
    errorvec[ndx] = error
    plt.plot(xvec,yvec,linewidth=2)
    plt.hold(True)
print error
plt.plot(xvec,ytrue,linewidth=2)
plt.legend('n=100')
plt.show()

