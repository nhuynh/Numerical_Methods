#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
import math

plt.close('all')

xi = 3.38
xf = 4.83
n = 1000

def f(x):
	myout = math.cos(math.pow(x,2))
	return myout


xplot = np.linspace(xi,xf,n)
dx = (xf-xi)/(n-1)

# Midpoint & Trapezoidal Methods
midpointsum = 0
trapsum = 0
for idx in range (0,n):
	a = xplot[idx]
	b = xplot[idx] + dx

	# Compute Midpoint
	xmid = (a+b)/2
	midpoint = dx*f(xmid)
	midpointsum+= midpoint

	# Compute Trapezoidal
	fa = f(a)
	fb = f(b)
	trap = dx*(fa+fb)/2
	trapsum += trap

print('Midpoint=',midpointsum)
print('Trapezoidal=',trapsum)


