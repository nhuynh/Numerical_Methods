#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
import math

# Solving Boundary Value Problems using FDM
# -0.06x *d2y/dx2 - 1.70*exp(x) *dy/dx + 0.32y = 1/(x+1)
# 1.33 < x < 4.19
# y(1.33) = 18.30
# yprime(4.19) = 2.06
plt.close('all')

ns = np.asarray([10,100,500,1000])

plt.figure()
for idx in range(0,len(ns)):
    n = ns[idx]
    xL = 1.33
    xU = 4.19
    xvec = np.linspace(xL,xU,n)

    dx = (xU-xL)/(n-1)

    A = np.zeros((n,n))
    RHS = np.zeros((n,1))

    A[0,0] = 1
    # Backwards differencing since 2nd B.C. is a yprime term
    A[n-1,n-2] = -1/dx  # A(n,n-1)
    A[n-1,n-1] = 1/dx   # A(n,n)
    RHS[0] = 18.30
    RHS[n-1] = 2.06


    for k in range (1,n-1):
        x = xvec[k]
        ypp = -0.06*x
        yp  = 1.70*np.exp(x)
        y   = 0.32
        A[k,k] = (-2*ypp)/(math.pow(dx,2)) + y
        A[k,k-1] = ypp/(math.pow(dx,2)) - yp/(2*dx)
        A[k,k+1] = ypp/(math.pow(dx,2)) + yp/(2*dx)
        RHS[k]   = 1/(x+1)

    yvec = np.linalg.solve(A,RHS)
    plt.plot(xvec,yvec,linewidth=2)
    plt.hold(True)
plt.xlabel('x')
plt.ylabel('y')
plt.legend(('n=10','n=100','n=500','n=1000'))