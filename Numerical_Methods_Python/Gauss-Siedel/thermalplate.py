import numpy as np
import matplotlib.pyplot as plt
import scipy as sci
from mpl_toolkits.mplot3d import Axes3D
import math


# Plate Dimensions
L = 40.464
w = 17.052
l1 = 31.5619
l2 = 3.92196
R = 1.96098

# Temperature Boundary Conditions
Tupper = 227
Tleft = 111
Tright = 249*1.5
Tbottom = 50
Tw = 0

nx = 75    # of x grid points
ny = 60    # of y grid points

Lx = np.linspace(0,L,nx)
Ly = np.linspace(0,w,ny);

dx = L / (nx-1);
dy = w / (ny-1);

plate = np.zeros((ny,nx));

plate[:,0] = Tleft
plate[:,nx-1] = Tright
plate[0,:] = Tupper
plate[ny-1,:] = Tbottom


errormat = np.zeros((ny,nx))
error_val = 100
counter = 0

while error_val > 0.0001:
    for j in range(1,ny-1):
        for i in range(1,nx-1):
            alfa = plate[j,i+1] + plate[j,i-1]
            beta = plate[j+1,i] + plate[j-1,i]
            
            Told = plate[j,i]
            Rtilde = np.sqrt(math.pow(Lx[i]-l1,2) + math.pow(Ly[j] - l2,2))         
            if (Rtilde < R) : 
                plate[j,i] = Tw                
                
            plate[j,i] = (alfa*math.pow(dy,2) + beta*math.pow(dx,2)) / (2*(dy*dy + dx*dx))
            
            Tnew = plate[j,i]

                        
            
            errormat[j,i] = np.abs(Tnew - Told)/Tnew
            
    error_val = np.max(np.max(errormat))
    counter += 1
    print "Error value = ", error_val
    print "Current iteration = ", counter

# Let's mesh our lengths of the plate

[Lxx, Lyy] = np.meshgrid(Lx,Ly)
    
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot_surface(Lxx,Lyy,plate, cmap = plt.cm.hsv)
plt.title('Temperature Distribution')
plt.xlabel('Length')
plt.ylabel('Width')
fig.colorbar
plt.show()



