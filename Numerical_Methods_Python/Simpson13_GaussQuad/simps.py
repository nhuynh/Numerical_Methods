#!usr/bin/python

import numpy as np
import matplotlib as plt
import math

def myfunc(x):
    y =math.sqrt(25 + 1.8*math.pow(x,2))
    return y

a = 0
b = 10.0
n = 525
dx = (b-a)/(n-1)
fa = myfunc(a)
fb = myfunc(b)

xvec = np.linspace(a,b,n)

foddsum = 0
# odds
for idx in range(2,n-2,2):
    fodds = 4*myfunc(xvec[idx])
    foddsum += fodds
    
fevensum = 0
# odds
for idx in range(1,n-1,2):
    fevens = 2*myfunc(xvec[idx])
    fevensum += fevens
    
myintegral = ((b-a)/n)*(fa + foddsum + fevensum + fb)/3
print(myintegral)