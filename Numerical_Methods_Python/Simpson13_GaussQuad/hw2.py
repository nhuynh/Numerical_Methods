#!/usr/bin/python

import numpy as np
import math
from getint import solve_gauss
def f(x):
    myout = math.sqrt(25.30 + 1.22*pow(x,2))
    return myout

xL = 5.24
xU = 10.77
n = 77
h = (xU-xL)/n
#--------- SIMPSON'S 1/3 RULE ---------#

xvec = np.linspace(xL,xU,n)
fa = f(xL)  # f(x0), first point
fb = f(xU)  # f(xN), last point

# Odd terms
# The loop looks weird because 0 is the first entry so the first
# odd term is 2, 4, etc. Super weird lol
oddsum = 0
for idx in range(2,n-1,2):
    xodds = xvec[idx]
    fodds = f(xodds)
    oddsum += fodds

# Even terms    
evensum = 0
for idx in range(1,n-2,2):
    xeven = xvec[idx]
    feven = f(xeven)
    evensum += feven
    
# Bringing it all together

Integral_Simp = h*(fa + 4*oddsum + 2*evensum + fb)/3
print('Integral using Simpson`s 1/3 Rule=',Integral_Simp)

#--------- GAUSS-LEGENDRE QUADRATURE ---------#

def getxtil(x,A,B):
    myout = A*x + B
    return myout

A = (xU-xL)/2
B = (xU+xL)/2
J = A

# For the nth number of points user wants to evaluate with
npoints = 2

gauss_weights = solve_gauss(npoints)

xn = gauss_weights[:,0]
Wn = gauss_weights[:,1]

gauss_sum = 0
for idx in range (0,npoints):
    xtil = getxtil(xn[idx],A,B)
    gauss_sum += J*Wn[idx]*f(xtil)

print('Integral using Gauss` Quadrature',gauss_sum)



