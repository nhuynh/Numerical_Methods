#!usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import math

plt.close('all')

# Plot my function
# This code is based on bisectionmethod.m in the path:
# ~/TA/ME328_F17/Week3/thursday
xL = 0
xU = 10
n = 100
xvec = np.linspace(xL,xU,n)
yvec = np.zeros((n,1))

def f(x):
    myout = 0.5*(x**2) + x - 5
    return myout

def fprime(x):
    myderiv = x + 1
    return myderiv

## Let's plot the function to see where the roots are

for idx in range(0,n):
    yvec[idx] = f(xvec[idx])

plt.figure()
plt.plot(xvec,yvec,'k-',linewidth=2)
plt.hold(True)
plt.plot([xL,xU],[0,0],'r--',linewidth=2)
plt.show()