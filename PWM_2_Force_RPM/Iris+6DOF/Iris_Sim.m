% 6DOF Simulation for Iris+ Quadrotor
clear;clc;close all;
load_inputs;
% global MS_ROLL MS_PITCH MS_YAW time_pilot
global pwm1 pwm2 pwm3 pwm4 pilot_time
global MS1 MS2 MS3 MS4 thrustvec forcevec
global FXaero FYaero FZaero MXaero MYaero MZaero

time  = linspace(sim.ti,sim.tf,sim.n);
tspan = [sim.ti, sim.tf];

x0 = 0;
y0 = 0;
z0 = -50;
phi0 = 0;
theta0 = 0;
psi0 = 0;
u0 = 0;
v0 = 0;
w0 = 0;
p0 = 0;
q0 = 0;
r0 = 0;

addpath('/home/nghia/Documents/Masters/Research/BlackBox/ode/')
addpath('/home/nghia/Documents/Masters/Research/TowedSystems/Flight_Tests/9_22_17-Parameter_Testing')
addpath('/home/nghia/Documents/Gitlab/PWM_2_Force_RPM/Iris+6DOF/')

expr  = dlmread('Quad_Data.out');    % Experiment File
simu = dlmread('States.OUT');        % Simulation File


r2d  = 180/pi;
m2ft = 3.28084;

% Determine start point when GPS receives signal
ir.time = expr(:,1);
itr = 1;

% We say 180 because that's when GPS timer kicks in
while (abs(ir.time(itr)) < 180)
    itr = itr + 1;
end


% Reads from Iris file
% We only want the file whenever it starts taking GPS data

ir.time = expr(itr:end,1);        % Time (hrs)
% ir.lat  = expr(itr:end,2);
% ir.lon  = expr(itr:end,3);
% ir.z    = expr(itr:end,4)*m2ft;   % Altitude (m to ft)
ir.phi  = expr(itr:end,2)*r2d;    % Roll Angle (rad)
ir.theta= expr(itr:end,3)*r2d;    % Pitch Angle (rad)
ir.psi  = expr(itr:end,4)*r2d;    % Yaw Angle (rad)
% ir.u    = expr(itr:end,8)*m2ft;   % Lateral Speed (m/s to ft/s)
% ir.v    = expr(itr:end,9)*m2ft;   % Longitudinal Speed (m/s to ft/s)
% ir.w    = expr(itr:end,10)*m2ft;  % Vertical Speed (m/s to ft/s)
ir.p    = expr(itr:end,5);
ir.q    = expr(itr:end,6);
ir.r    = expr(itr:end,7);
ir.TL   = expr(itr:end,11);       % Top-Left Rotor,    1
ir.TR   = expr(itr:end,12);       % Top-Right Rotor,     2
ir.BR   = expr(itr:end,13);       % Bottom-Right Rotor, 3
ir.BL   = expr(itr:end,14);       % Bottom-Left Rotor,  4

flight_time = ir.time(end) - ir.time(1);
flight_time = linspace(0,flight_time,length(ir.time));

pilot_time = ir.time;   % ir.time for experiment time, flight time just starts it at 0
pwm1 = ir.TL;
pwm2 = ir.TR;
pwm3 = ir.BR;
pwm4 = ir.BL;

pwm = [pwm1 pwm2 pwm3 pwm4];
state0 = [x0 y0 z0 phi0 theta0 psi0 u0 v0 w0 p0 q0 r0];
state = zeros(length(time),length(state0));
state(1,:) = state0;

% My RK4
% for k = 1:length(time)-1
%     xk = state(k,:);
%     tk = time(k);
%     k1 = Derivatives(tk,xk);
%     k2 = Derivatives(tk+sim.dt/2,xk+k1*sim.dt/2);
%     k3 = Derivatives(tk+sim.dt/2,xk+k2*sim.dt/2);
%     k4 = Derivatives(tk+sim.dt,xk+k3*sim.dt);
%     phi = (1/6)*(k1 + 2*k2 + 2*k3 + k4);
%     state(k+1,:) = state(k,:) + phi'*dt;
% 
% end
% tsim = time;
% 
% stateout = state;

% Using built-in RK4 solver with adaptive time stepping
% Ok nvm I'm using Dr. C's RK4 solver instead because I can give a discrete
% time step.
% ok nvm again I need to input the pwm signal so fk it i'm doing it this
% way
[tsim, stateout] = odeK4(@Derivatives,tspan,state0',sim.dt);

% Just to see the output of the MS values -- they are the same as the input
MS1p = zeros(1,length(pwm1));
MS2p = zeros(1,length(pwm2));
MS3p = zeros(1,length(pwm3));
MS4p = zeros(1,length(pwm4));

% Forces too
f1p = zeros(1,length(tsim));
f2p = zeros(1,length(tsim));
f3p = zeros(1,length(tsim));
f4p = zeros(1,length(tsim));
FVEC = zeros(3,length(tsim));
MVEC = zeros(3,length(tsim));

for idx = 1:length(tsim)
    dxdt = Derivatives(time(idx),stateout(:,idx));
    MS1p(idx) = MS1;
    MS2p(idx) = MS2;
    MS3p(idx) = MS3;
    MS4p(idx) = MS4;
    
    f1p(idx) = forcevec(1);
    f2p(idx) = forcevec(2);
    f3p(idx) = forcevec(3);
    f4p(idx) = forcevec(4);
    
    FVEC(1,idx) = FXaero;
    FVEC(2,idx) = FYaero;
    FVEC(3,idx) = FZaero;

    MVEC(1,idx) = MXaero;
    MVEC(2,idx) = MYaero;
    MVEC(3,idx) = MZaero;
    
end

mstest = fopen('microseconds.txt','w');
formatspec = '%2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f\n';
fprintf(mstest,'PWM1 \t \t Interpolated \t \t PWM2 \t \t Interpolated \t \t PWM3 \t \t Interpolated \t \t PWM4 \t \t Interpolated \n');
for idx = 1:length(time)
    fprintf(mstest,formatspec,pwm1(idx),MS1p(idx),pwm2(idx),MS2p(idx),pwm3(idx),MS3p(idx),pwm4(idx),MS4p(idx));
end
fclose(mstest);

forceoutput = fopen('forcesmoments.txt','w');
formatspec = '%2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \n';

for idx = 1:length(tsim)
    fprintf(forceoutput,formatspec,tsim(idx),FVEC(:,idx),MVEC(:,idx),f1p(idx),f2p(idx),f3p(idx),f4p(idx));
end
fclose(forceoutput);


MFS = 16;           % My Font Size
MFN = 'Ubuntu';     % My Font Name
LWS = 2;            % LineWidth Size
r2d = 180/pi;

% figure()
% subplot(2,1,1)
% plot(tsim,pwm1)
% hold on
% plot(tsim,MS1p)
% legend('True PWM Signal','Interpolated PWM Signal')
% title('Input PWM Signal')
% 
% subplot(2,1,2)
% plot(tsim,f1p)
% title('Corresponding Thrust Vector')
% suptitle('Top Left(1)')
% 
% figure()
% subplot(2,1,1)
% plot(tsim,pwm2)
% hold on
% plot(tsim,MS2p)
% legend('True PWM Signal','Interpolated PWM Signal')
% title('Input PWM Signal')
% 
% subplot(2,1,2)
% plot(tsim,f2p)
% title('Corresponding Thrust Vector')
% suptitle('Top Right(2)')
% 
% figure()
% subplot(2,1,1)
% plot(tsim,pwm3)
% hold on
% plot(tsim,MS3p)
% legend('True PWM Signal','Interpolated PWM Signal')
% title('Input PWM Signal')
% 
% subplot(2,1,2)
% plot(tsim,f3p)
% title('Corresponding Thrust Vector')
% suptitle('Bottom Right(3)')
% 
% figure()
% subplot(2,1,1)
% plot(tsim,pwm4)
% hold on
% plot(tsim,MS4p)
% legend('True PWM Signal','Interpolated PWM Signal')
% title('Input PWM Signal')
% 
% subplot(2,1,2)
% plot(tsim,f4p)
% title('Corresponding Thrust Vector')
% suptitle('Bottom Left(4)')

x = stateout(1,:);
y = stateout(2,:);
z = stateout(3,:);
phi = stateout(4,:);
theta = stateout(5,:);
psi = stateout(6,:);
u = stateout(7,:);
v = stateout(8,:);
w = stateout(9,:);
p = stateout(10,:);
q = stateout(11,:);
r = stateout(12,:);
% length(tsim)
% length(x)

% fid = fopen('States.OUT','w');
% kobe = '%2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \t %2.8f \n';
% for kdx = 1:length(tsim)
%     fprintf(fid,kobe,tsim(kdx),stateout(kdx,:));
% end
% fclose(fid);

% Plotting
% MFS = 16;           % My Font Size
% MFN = 'Ubuntu';     % My Font Name
% LWS = 2;            % LineWidth Size
% plot_iris;

figure()
plot(tsim,x,'k-','LineWidth',LWS)
hold on
plot(tsim,y,'r-','LineWidth',LWS)
plot(tsim,-z,'b-','LineWidth',LWS)
xlabel('Time (s)','FontName',MFN,'FontSize',MFS)
ylabel('Position (ft)','FontName',MFN,'FontSize',MFS)
title('x,y,z','FontName',MFN,'FontSize',MFS)
legend('x','y','z')

figure()
% plot(tsim,phi*r2d,'k-','LineWidth',LWS)
% hold on
plot(tsim,theta*r2d,'r-','LineWidth',LWS)
% plot(tsim,psi*r2d,'b-','LineWidth',LWS)
xlabel('Time (s)','FontName',MFN,'FontSize',MFS)
ylabel('Degrees','FontName',MFN,'FontSize',MFS)
title('\theta','FontName',MFN,'FontSize',MFS)
% title('\phi,\theta,\psi','FontName',MFN,'FontSize',MFS)
% legend('\phi','\theta','\psi')

figure()
plot(tsim,u,'k-','LineWidth',LWS)
% hold on
% plot(tsim,v,'r-','LineWidth',LWS)
% plot(tsim,-w,'b-','LineWidth',LWS)
xlabel('Time (s)','FontName',MFN,'FontSize',MFS)
ylabel('Velocity (ft/s)','FontName',MFN,'FontSize',MFS)
title('u,v,w','FontName',MFN,'FontSize',MFS)
% legend('u','v','w')

figure()
% plot(tsim,p*r2d,'k-','LineWidth',LWS)
% hold on
plot(tsim,q*r2d,'r-','LineWidth',LWS)
% plot(tsim,r*r2d,'b-','LineWidth',LWS)
xlabel('Time (s)','FontName',MFN,'FontSize',MFS)
ylabel('Angular Velocity (deg/s)','FontName',MFN,'FontSize',MFS)
title('p,q,r','FontName',MFN,'FontSize',MFS)
% legend('p','q','r')

