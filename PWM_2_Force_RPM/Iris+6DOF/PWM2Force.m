function [MS_1, MS_2, MS_3, MS_4, MS_0] = PWM2Force(pwm)
% This function converts an input PWM signal (microseconds) to output a
% resulting force from a quadrotor rotor.


W0 = 0.765;
rpm2rads = 2*pi/60;

W = [0.765 0.723 0.686 0.644 0.606 0.553 0.454 0.381 0.291 0.195]; 
us = [1134 1199 1240 1280 1313 1354 1411 1451 1500 1535];

rpm = [0 2919 3623 4881 6375 7170 8219 9305 10366 11397]/2; % since there's two blades
ms  = [1134 1174 1205 1247 1304 1330 1384 1420 1474 1523];

pwm2rpm = polyfit(ms,rpm,1);

omega_max = 11970;%1250/rpm2rads
pwm2rpm_max = polyval(pwm2rpm,1900);

kq = omega_max/pwm2rpm_max;         % PWM to rpm constant
% So this will be the true value of the angular velocity. From here we tune
% our kt and C_T constants to match
rpm_hover = polyval(pwm2rpm,1589.07068953)*rpm2rads*kq;

%%%This will give us a polynomial that we can use to evaluate thrust
%W=f(us)

pwm2F = polyfit(us,W,2);
MS_h = 1589.07068953;

MS_0 = polyval(pwm2F,MS_h);     % Ensures hovering

% These forces will be a function of the input pwm
% Also added logic for 0 forces if pwm = 0. If not then then constant term
% will always exist and we don't want that
if pwm < 1170
    MS_ROLL  = 0*polyval(pwm2F,pwm);
    MS_PITCH = 0*polyval(pwm2F,pwm);
    MS_YAW   = 0*polyval(pwm2F,pwm);
else
    MS_ROLL  = 0*polyval(pwm2F,pwm);
    MS_PITCH = 0*polyval(pwm2F,pwm);
    MS_YAW   = 0*polyval(pwm2F,pwm);
end

MS_1 = polyval(pwm2F,pwm(1));
MS_2 = polyval(pwm2F,pwm(2));
MS_3 = polyval(pwm2F,pwm(3));
MS_4 = polyval(pwm2F,pwm(4));