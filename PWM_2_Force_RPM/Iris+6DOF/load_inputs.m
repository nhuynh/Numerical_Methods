% 3DR Iris+ Simulation Parameters

%% Simulation
sim.ti = 194.7;15;              % Initial time (s)
sim.tf = 196.1;             % Final time (s)
sim.n = 1760;%890;            % Number of data points
sim.dt = (sim.tf-sim.ti)/(sim.n-1);  % Step size

%% Environment
env.g = 32.2;            % Gravity (ft/s^2)
env.rho = 0.0023769;     % Air density (slug/ft^3)

%% Mass Properties
mass.W = 2.82635;        % Weight (lbs)
mass.m = mass.W/env.g;	 % Mass (slugs)
mass.W0 = 0.765;         % Initial weight of one leg (lbs), from thrust test

mass.Ixx = 0.564783;%0.11524;      % Inertia (lb*ft^2)
mass.Iyy = 0.187502;      % Inertia (lb*ft^2)
mass.Izz = 0.71903;%0.20885;      % Inertia (lb*ft^2)
mass.Ixy = 0;            % Inertia (lb*ft^2)
mass.Ixz = 0;            % Inertia (lb*ft^2)
mass.Iyz = 0.00576*0;    % Inertia (lb*ft^2)
mass.Ir  = 0.002089;     % Inertia about the local rotor axis (lb*ft^2)
mass.I = [mass.Ixx, 0, 0; 0 mass.Iyy 0; 0 0 mass.Izz];
mass.Iinv = [1/mass.Ixx, 0, 0; 0 1/mass.Iyy 0; 0 0 1/mass.Izz];

%% Geometry
geom.lphi12 = 0.7283465; % Distance from roll axis (ft), front two rotors (1 & 2)
geom.lphi34 = 0.6758530; % Distance from roll axis (ft), back two rotors (3 & 4)
geom.ltheta = 0.4461942; % Distance from pitch axis (ft). Assumed 12 & 34 to be the same since it was only off by 1 cm
geom.ltheta12 = 0.4461942;
geom.ltheta34 = 0.4265020;
geom.R      = 0.375;     % Rotor radius (ft)

%% Aerodynamics bs
aero.Alc   = 0.017;      % Aspect geometry constant, cosine component
aero.Als   = 0.017;      % Aspect geometry constant, sine component
aero.dxD   = 0.00895;    % Drag coefficient x-direction
aero.dyD   = 0.00994;    % Drag coefficient y-direction
aero.C_T   = 0.0614730899895676;   % Motor thrust constant (lbf-ft); tune from rpm vs pwm test
aero.C_Tau = 0.02725;    % Motor torque constant (lbf-ft)
aero.kt = aero.C_T*(4*env.rho*(geom.R^4))/(pi^2);   % Aerodynamic thrust constant
aero.b  = aero.C_Tau*(4*env.rho*(geom.R^5))/(pi^3); % Aerodynamic torque constant
aero.kf = 1.0546800000142;         % Non-dimensional constant to fit force as a function of pwm signal

%% Controls
cs.omega_max = 1250;                    % Max rotor speed (rad/s)
cs.omega_0 = sqrt(mass.W/(4*aero.kt));  % Angular velocity for hover (rad/s)
cs.MS_min = 1134;
cs.MS_max = 1900;