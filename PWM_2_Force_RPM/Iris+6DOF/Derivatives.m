function stateout = Derivatives(time,state)
load_inputs;
% global MS_ROLL MS_PITCH MS_YAW time_pilot
global pwm1 pwm2 pwm3 pwm4 pilot_time
global MS1 MS2 MS3 MS4 thrustvec forcevec
global FXaero FYaero FZaero MXaero MYaero MZaero 

ptp = state(4:6);
uvw = state(7:9);
pqr = state(10:12);

phi = ptp(1);
theta = ptp(2);
psi = ptp(3);
u = uvw(1);
v = uvw(2);
w = uvw(3);
p = pqr(1);
q = pqr(2);
r = pqr(3);

%% Kinematics

T3 = [cos(psi) -sin(psi) 0;sin(psi) cos(psi) 0;0 0 1];
T2 = [cos(theta) 0 sin(theta); 0 1 0; -sin(theta) 0 cos(theta)];
T1 = [1 0 0;0 cos(phi) -sin(phi); 0 sin(phi) cos(phi)];
T321 = T3*T2*T1;
xyzdot = T321*uvw;

H = [1 sin(phi)*tan(theta) cos(phi)*tan(theta);...
     0 cos(phi) -sin(phi); ...
     0 sin(phi)/cos(theta) cos(phi)/cos(theta)];
ptpdot = H*pqr;

%% Dynamics

% Gravity Contributions
FGRAV = T321' * [0;0;mass.W];
MGRAV = [0;0;0];


MS1 = interp1(pilot_time,pwm1,time);
MS2 = interp1(pilot_time,pwm2,time);
MS3 = interp1(pilot_time,pwm3,time);
MS4 = interp1(pilot_time,pwm4,time);
PWM = [MS1 MS2 MS3 MS4];
[MS_1, MS_2, MS_3, MS_4, MS_0] = PWM2Force(PWM);



% pwm
% MS_PITCH
% MS_0
forcevec(1) = MS_1;  %MS_0 + MS_ROLL + MS_PITCH + MS_YAW;
forcevec(2) = MS_2;  %MS_0 - MS_ROLL + MS_PITCH - MS_YAW;
forcevec(3) = MS_3;  %MS_0 - MS_ROLL - MS_PITCH + MS_YAW;
forcevec(4) = MS_4;  %MS_0 + MS_ROLL - MS_PITCH - MS_YAW;
forcevec = forcevec*aero.kf;        % Multipled with another constant

% sumforce = (sum(forcevec) - mass.W0);
%*aero.kt
% Not so sure about this part so let's revisit
for idx = 1:4
    thrustvec(idx) = -(forcevec(idx) - mass.W0)/(cosd(11.365));
end

thrust = sum(thrustvec);
% thrust = -(sum(forcevec) - 4*mass.W0)/(cosd(11.365));

% OM[E]G[A] I see why we don't have to do PWM --> Omega. Lmao. Man I'm dumb
for fk = 1:4
    omega(fk) = sqrt(forcevec(fk)/aero.kt);
end

sumomega = sum(omega);

omegar = omega(1) - omega(2) + omega(3) - omega(4);
FXaero = -thrust*(((aero.Alc)/(sumomega*geom.R) + aero.dxD)*u -((aero.Als)/(sumomega*geom.R))*v);
FYaero = -thrust*(((aero.Als)/(sumomega*geom.R))*u +((aero.Alc)/(sumomega*geom.R) + aero.dyD)*v);
FZaero = -thrust;

FAERO = [FXaero;FYaero;FZaero];

Gamma(1) =  mass.Ir * omegar * q;
Gamma(2) = -mass.Ir * omegar * p;
Gamma(3) =  0;

MXaero = Gamma(1) + geom.lphi12*(thrustvec(1) - thrustvec(2)) + geom.lphi34*(thrustvec(4) - thrustvec(3));
MYaero = Gamma(2) + (geom.ltheta12*(thrustvec(1) + thrustvec(2)) + geom.ltheta34*(- thrustvec(4) - thrustvec(3)));
MZaero = Gamma(3) + aero.b*(omega(1)^2 - omega(2)^2 + omega(3)^2 - omega(4)^2);

MAERO = [MXaero;MYaero;MZaero];

FTOTAL = FAERO + FGRAV;
MTOTAL = MAERO + MGRAV;

% Solve for accelerations
pqrskew = [0 -r q;r 0 -p;-q p 0];

uvwdot = FTOTAL/mass.m - pqrskew*uvw;
pqrdot = mass.Iinv*(MTOTAL - pqrskew*mass.I*pqr);

stateout = [xyzdot;ptpdot;uvwdot;pqrdot];
